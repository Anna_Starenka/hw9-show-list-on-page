// --- 1---

// Створити новий HTML тег на сторінці можна за допомогою метода - document.createElement(tag)
// const newElement = document.createElement('div');

// --- 2 ---
// Перший параметр функції insertAdjacentHTML вказує, куди по відношенню до елемента робити вставку
// Значення має бути одним із наступних:
// - beforebegin - вставити html безпосередньо перед елементом
// - afterbegin - вставити html на початок елемента
// - beforeend - вставити html в кінець елемента
// - afterend - вставити html безпосередньо після елемента

// --- 3 ---

// Для видалення елемента є метод node.remove()
// Також всі методи вставки автоматично видаляють вузли зі старих місць





function displayList(arr, pos= document.body) {
    
    let ul = document.createElement('ul')

    arr.forEach(element => {
        let li = document.createElement('li')
        li.append(element)
        ul.append(li)
    });
    pos.prepend(ul)
    setTimeout(()=>{
        ul.remove()
    },3000)
   
}

displayList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"])




